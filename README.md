# Briefcase backend

Versión node: `12.16.1`
Version mongoDB: `4.2.3`


## Configuración del proyecto
```
npm install
```

### Ejecute proyecto
```
npm run serve
```

Hecho con ♥ por [Jose Florez](www.joseflorez.co)
